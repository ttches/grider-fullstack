import { FETCH_USER } from './types';

export const fetchUser = () => {
  return async (dispatch) => {
    const response = await fetch('/api/current_user', { credentials : 'same-origin' });
    let data = false;
    try {
      data = await response.json();
    }
    catch(err) {
      console.log('No user logged in');
    }
    dispatch({ type: FETCH_USER, payload: data });
  };
};

export const handleToken = (token) => {
  return async (dispatch) => {
    const response = await fetch('/api/stripe',
    {
      method: 'POST',
      body: JSON.stringify(token),
      credentials: 'same-origin',
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    });
    if (!response.ok) return;
    const data = await response.json();
    dispatch({ type: FETCH_USER, payload: data });
  };
};